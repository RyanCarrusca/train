#include "train.h"
#include "city.h"
#include "passenger.h"
#include "station.h"


//#define CITY_FILE "map1514661237.txt"
#define CITY_FILE "assets/vancouverStations.txt"

//g++ -Wall -o main -std=c++11 -I/home/ryan/Programming/train *.cc  -lsfml-graphics -lsfml-window -lsfml-system

 #define fromFile 0
 #define genMap 1
 
 
int main() {
	srand((unsigned)time(NULL));
	City city;
	
	int mode = genMap;
	
	
	if (mode == fromFile){
		if (city.initializeFromFile(CITY_FILE)) {
			return EXIT_FAILURE;
		}
	}
	else if (mode == genMap){
		city.generateMap((unsigned)time(NULL)); 
	}
	
	city.run();

	return EXIT_SUCCESS;
}
