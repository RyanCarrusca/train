#include "text.h"



Text::Text() {
	fontFile = FONT_FILE;

	if (!font.loadFromFile(fontFile)) {
		//return EXIT_FAILURE;
	}

	blankPassText.setFont(font);
	blankPassText.setColor(sf::Color::Black);
	blankPassText.setCharacterSize(TEXT_SIZE_PASSENGER_INFO);

	trainText.setFont(font);
	trainText.setColor(sf::Color::Yellow);
	trainText.setCharacterSize(TEXT_SIZE_TRAIN_PASSENGERS);
	trainText.setStyle(sf::Text::Bold);

	passTot.setFont(font);
	passTot.setColor(sf::Color::Black);
	passTot.setCharacterSize(TEXT_SIZE_TOTAL_PASSENGERS);

	testBoxText.setFont(font);
	testBoxText.setColor(sf::Color::Black);
	testBoxText.setCharacterSize(25);

}
