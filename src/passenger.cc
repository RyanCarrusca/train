#include "passenger.h"



Passenger::Passenger() {};


Passenger::Passenger(sf::Vector2f pos, Station st, Station end) {
	position = pos;
	start = st;
	destination = end;

	currentStation = st;
	currentTrain.assign(2, -1);

	status = PASSENGER_WAITING;
	shape = sf::CircleShape(7, 4);
	shape.setFillColor(sf::Color::Green);
	shape.setOrigin(3.5, 3.5);
	shape.setPosition(position.x, position.y);
}


Passenger::Passenger(std::vector <std::vector <Station*> > &allLineStations, int sn) {
	Station tempStat = getRandStation(allLineStations);
	Station tempStat2 = getRandStation(allLineStations);

	position = tempStat.position;
	start = tempStat;
	destination = tempStat2;
	findTransfer(start, destination, transfer, next, allLineStations);

	currentStation = tempStat;
	currentTrain.assign(2, -1);

	status = PASSENGER_WAITING;
	shape = sf::CircleShape(7, 4);
	shape.setFillColor(PASSENGER_COLOUR);
	shape.setOrigin(3.5, 3.5);
	shape.setPosition(position.x, position.y);

	ssn = sn;
}


/*
*Generates passenger info text
*/
std::string Passenger::genStatusText(const std::vector<std::string> &colors, const std::vector <std::vector <Train> > &allLineTrains) {
	if (status == PASSENGER_RIDING || status == PASSENGER_NEARLY_DEST || status == PASSENGER_NEARLY_TRANSFER) {
		return std::to_string(ssn) +
			" on train " + colors.at(currentTrain.at(0)) + "-" + std::to_string(currentTrain.at(1)) +
			" at " + allLineTrains.at(currentTrain.at(0)).at(currentTrain.at(1)).next->name +
			" going to " + transfer.name +
			" with final destination " + destination.name;
	}
	else if (status == PASSENGER_WAITING) {
		return std::to_string(ssn) +
			" waiting for " + colors.at(next) +
			" at " + currentStation.name +
			" to " + transfer.name +
			" with final destination " + destination.name;
	}
	else {
		return "";
	}

}


/*
*finds out where the passengers need to go, what train to catch next, or what station to stop at.
*returns the new number of delivered passengers
*/
void Passenger::move(int &passengerTotal, std::vector <std::vector <Train> > &allLineTrains, std::vector<std::vector<Station*> > allLineStations) {
	unsigned int k;

	if (status == PASSENGER_WAITING) {
		for (k = 0; k < allLineTrains.at(next).size(); k++) {
			if (allLineStations.at(next).at(allLineTrains.at(next).at(k).segment)->name == currentStation.name) {
				currentTrain.at(0) = next;
				currentTrain.at(1) = k;
				status = PASSENGER_RIDING;
				break;
			}
		}
	}
	else {
		std::string curTrainNextName = allLineTrains.at(currentTrain.at(0)).at(currentTrain.at(1)).next->name;

		if (status == PASSENGER_RIDING) {

			//for when their stop is one station away
			if (curTrainNextName == destination.name) {
				status = PASSENGER_NEARLY_DEST;
			}
			else if (curTrainNextName == transfer.name) {
				status = PASSENGER_NEARLY_TRANSFER;
			}
		}
		else if (status == PASSENGER_NEARLY_DEST && curTrainNextName != destination.name) {
			//if the next station is not their destination when there are in this status, they have just passed their destination
			status = PASSENGER_ARRIVED;
			passengerTotal++;
		}
		else if (status == PASSENGER_NEARLY_TRANSFER && curTrainNextName != transfer.name) { //same as above but with transfers instead of destinations
			shape.setPosition(transfer.position.x, transfer.position.y - 15);
			status = PASSENGER_WAITING;
			currentStation = transfer;
			findTransfer(currentStation, destination, transfer, next, allLineStations);
		}
	}

	//return passengerTotal;
}


