#pragma once

#include <SFML/Graphics.hpp>
#include <cmath>
#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <time.h>
#include <stdlib.h>
#include <map>



#ifndef TEXT_H
#define TEXT_H

#define FONT_FILE "assets/Raleway-Regular.ttf"

#define TEXT_SIZE_PASSENGER_INFO 9
#define TEXT_SIZE_TRAIN_PASSENGERS 20
#define TEXT_SIZE_TOTAL_PASSENGERS 40

class Text {
public:
	std::string fontFile;

	sf::Text blankPassText, trainText, passTot;	
	sf::Font font;
	
	sf::Text testBoxText;

	Text();


};



#endif
