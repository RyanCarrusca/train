#include "train.h"

//https://www.nidcr.nih.gov/AboutUs/VisitingNIDCR/Maps/PublishingImages/metro-map2016.jpg


Train::Train(sf::RectangleShape shap, std::string lin){
	shape = shap;
	line = lin;
	segment = 0;
	passengerNumber = 0;
}


//initializes starting position, angle, and direction of a train
void Train::setup(const sf::VertexArray &lineVertices) {
	std::vector <int> alreadyStarting;
	bool redo = true;
	int startingSegment;
	double x1, y1, x2, y2;

	while (redo) {
		startingSegment = rand() % (lineVertices.getVertexCount() - 1);

		redo = isInVector(alreadyStarting, startingSegment);

		alreadyStarting.push_back(startingSegment);
	}

	segment = startingSegment;

	x1 = lineVertices[segment].position.x;
	y1 = lineVertices[segment].position.y;
	x2 = lineVertices[segment + 1].position.x;
	y2 = lineVertices[segment + 1].position.y;

	xPos = x1;
	yPos = y1;
	shape.setPosition((float)x1, (float)y1);

	curAngle = atan2(y2 - y1, x2 - x1);
	curDist = hypot(x2 - x1, y2 - y1);
	curPos = 0;
}


void Train::draw(sf::RenderWindow &window) {
	window.draw(shape);
}


void Train::move(const sf::VertexArray &lineVertices, const std::vector <Station*> lineStations) {
	double x1, y1, y2, x2;

	xPos = shape.getPosition().x;
	yPos = shape.getPosition().y;
	passengerNumber = 0;

	if (curPos <= curDist) {
		curPos += 1;

		xPos += cos(curAngle);
		yPos += sin(curAngle);

		shape.setPosition((float)xPos, (float)yPos);

	}
	else {
		segment += dir;

		//at the end of the line, switch direction
		if (segment + dir >= lineVertices.getVertexCount()) {
			dir = -1 * dir;
		}
		x1 = lineVertices[segment].position.x;
		y1 = lineVertices[segment].position.y;

		x2 = lineVertices[segment + dir].position.x;
		y2 = lineVertices[segment + dir].position.y;

		curAngle = atan2(y2 - y1, x2 - x1);
		curDist = hypot(x2 - x1, y2 - y1);
		curPos = 0;

		next = lineStations.at(segment + dir);
	}
}


/*
* makes a vector of the specified number of trains all with the same shape and colour
*/
std::vector <Train> genLineTrains(int numTrains, sf::RectangleShape shape, std::string color) {
	std::vector <Train> trains (numTrains, Train(shape, color));

	return trains;
}


// checks to see if the given int is in the vector <int>
bool isInVector(const std::vector<int> &vect, int search) {
	unsigned int i;
	for (i = 0; i < vect.size(); i++)
		if (vect[i] == search)
			return true;

	return false;
}

