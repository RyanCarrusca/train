#pragma once

#include "station.h"





#ifndef TRAIN_H
#define TRAIN_H

#define TRAIN_WIDTH 40
#define TRAIN_HEIGHT 20

class Train {
public:
	double xPos;
	double yPos;
	double curDist;
	double curPos;
	double curAngle;
	unsigned int segment;
	Station* next;

	int dir = 1;
	int passengerNumber;

	sf::RectangleShape shape;
	sf::Text text;
	std::string line;

	Train(sf::RectangleShape, std::string);
	void setup(const sf::VertexArray &lineVertices);

	void draw(sf::RenderWindow &window);
	void move(const sf::VertexArray &lineVertices, const std::vector <Station*> lineStations);
};

std::vector <Train> genLineTrains(int numTrains, sf::RectangleShape shape, std::string color);
bool isInVector(const std::vector<int> &vect, int search);



#endif