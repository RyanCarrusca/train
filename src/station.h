#pragma once


#include <SFML/Graphics.hpp>
#include <cmath>
#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <time.h>
#include <stdlib.h>
#include <map>


#ifndef STATION_H
#define STATION_H


class Station {
public:
	int x;
	int y;
	sf::Vertex vert;
	sf::Vector2f position;
	std::string name;
	std::vector <int> lines;
	std::vector <int> linePos;

	Station* prev;
	double dist;

	Station();
	Station(sf::Vertex, std::string);
};


Station getRandStation(const std::vector <std::vector <Station*> > &allLineStations);
std::vector<int> alreadyStation(std::string, std::vector <std::vector <Station*> > allLineStations);

void findTransfer(Station start, Station end, Station &transfer, int &next, std::vector<std::vector<Station*> > allLineStations);


#endif