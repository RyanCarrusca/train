#include "city.h"

#define LINE_THICKNESS 5




static double Gradient[WINDOW_WIDTH / BOX_SIZE][WINDOW_HEIGHT / BOX_SIZE][2];

//y1j1b


City::City() {
	window.create(sf::VideoMode(WINDOW_WIDTH, WINDOW_HEIGHT), WINDOW_NAME);
	window.setFramerateLimit(FPS_MAX);

	passengerTotal = 0;
	stationTotal = 0;

	//basic train shape
	trainShape = sf::RectangleShape(sf::Vector2f(TRAIN_WIDTH, TRAIN_HEIGHT));
	trainShape.setOrigin(TRAIN_WIDTH / 2, TRAIN_HEIGHT / 2);

	trainTexture.loadFromFile(TRAIN_IMAGE); //if this load fails then the trains appear as white rectangles
	trainShape.setTexture(&trainTexture);
};


City::~City() {

	for (int i = allLineStations.size() - 1; i >= 0; i--) {
		for (auto it = allLineStations.at(i).begin(); it != allLineStations.at(i).end();) {
			it = allLineStations.at(i).erase(it);
		}
		allLineStations.at(i).clear();
	}
	allLineStations.clear();
}


//runs the main loop
void City::run(void) {
	int gameTick = 0, start = 0;
	sf::Event event;

	while (window.isOpen()) {
		while (window.pollEvent(event)) {
			switch (event.type) {
			case sf::Event::Closed:
				window.close();
				break;
			case sf::Event::KeyPressed:
				if (sf::Keyboard::isKeyPressed(sf::Keyboard::Return)) {
					start = 1 - start;
				}
			default: {
				break;
			}
			}
		}
		if (start) {
			continue;
		}

		window.clear(sf::Color::White);
		window.draw(mapSprite);


		//Passenger generation
		if (gameTick % PASSENGER_FREQUENCY == 0) {
			passengers.push_back(Passenger(allLineStations, gameTick / PASSENGER_FREQUENCY + 1));
			passengers.at(passengers.size() - 1).text = text.blankPassText;
		}

		//Passenger moving and info text drawing
		doPassengerMove(window);

		//train moving and drawing
		doTrainMove(window);

		//passenger drawing
		drawPassengers(window);

		//Train passenger text generation
		drawTrainPassengerNum(window);

		text.passTot.setString("Total: " + std::to_string(passengerTotal));
		window.draw(text.passTot);

		gameTick++;
		window.display();
	}
}


sf::Image generateImageBox(double x, double y) {
	double	p = perlin(x / SIMPLICITY / BOX_SIZE, y / SIMPLICITY / BOX_SIZE);

	sf::Color color(0, (sf::Uint8)(255 - WATERLEVEL * p), (sf::Uint8)(p*WATERLEVEL));
	//sf::Color color(0,  255 - p * 255, 255 * p );



	sf::Image image;
	image.create(BOX_SIZE, BOX_SIZE, color);
	return image;

}


sf::Image generateImage(int width, int height, int boxSize) {
	int x, y;
	sf::Image background, image;

	background.create(width, height, sf::Color::White);

	for (x = 0; x < width / boxSize; x++) {
		for (y = 0; y < height / boxSize; y++) {
			image = generateImageBox((double)x, (double)y);
			background.copy(image, x*boxSize, y*boxSize);
		}

	}

	return background;
};


/* makes a station line on the mapImage, and adds a line to allLineVertices, allLineNames, and allLineStations */
void City::generateStationLine(int max_dist_x, int min_dist_x, int max_dist_y, int min_dist_y, int numStations, sf::Color colour, std::string color, int trainsPerLine, int lineNum) {
	static sf::Image statImage, whiteSquare;

	statImage.create(LINE_THICKNESS * 3, LINE_THICKNESS * 3, colour);
	whiteSquare.create(LINE_THICKNESS, LINE_THICKNESS, sf::Color::White);

	statImage.copy(whiteSquare, LINE_THICKNESS, LINE_THICKNESS);

	static int EDGE_DIST = 25;
	static int step = 25;


	sf::VertexArray tempLineVertices(sf::LinesStrip, numStations);
	std::vector<std::string> tempLineNames(tempLineVertices.getVertexCount());
	std::vector<Station*> tempLineStations;
	std::vector<Train> tempLineTrains = genLineTrains(trainsPerLine, trainShape, color);

	sf::Vector2i size(mapImage.getSize());

	std::vector <int> AScoords;
	static std::vector <int> blank(2, -1);
	
	sf::Vector2i coords;
	do {
		coords = sf::Vector2i(randInt(EDGE_DIST, WINDOW_WIDTH * 2 / 3 - EDGE_DIST, step), randInt(EDGE_DIST, WINDOW_HEIGHT - EDGE_DIST, step));
	} while (isWater[coords.x / BOX_SIZE][coords.y / BOX_SIZE]);
	
	int dx, dy, cxi, cyi, i, dirx, diry;
	
	cxi = coords.x;
	cyi = coords.y;
	dirx = 1;
	diry = 1;


	//this generates number of lines between stations
	for (i = 0; i < numStations - 1; i++) {

		dx = randInt(min_dist_x, max_dist_x, step); //make sure max and min are divisible by step
		dy = randInt(min_dist_y, max_dist_y, step);

		coords.x += dirx * dx;
		coords.y += diry * dy;

		if (coords.x > size.x - EDGE_DIST || coords.x < EDGE_DIST) {
			dirx = -dirx;
			coords.x += 2 * dirx * dx;

			swap(max_dist_x, max_dist_y);
			swap(min_dist_x, min_dist_y);

			i--;
		}
		else if (coords.y > size.y - EDGE_DIST || coords.y < EDGE_DIST) {
			diry = -diry;

			coords.y += 2 * diry * dy;

			swap(max_dist_x, max_dist_y);
			swap(min_dist_x, min_dist_y);

			i--;
		}
		else if (isWater[coords.x / BOX_SIZE][coords.y / BOX_SIZE]) {
			i--;
		}
		else {
			mapImage.copy(statImage, cxi - 5, cyi - 5);
			lineBetween(cxi, cyi, coords.x, coords.y, mapImage, colour);


			tempLineVertices[i].position = sf::Vector2f((float)cxi, (float)cyi);
			tempLineNames.at(i) = std::to_string(cxi) + std::to_string(cyi);

			AScoords = alreadyStation(tempLineNames.at(i), allLineStations);
			if (AScoords == blank) {
				//make a new station
				tempLineStations.push_back(new Station(tempLineVertices[i], tempLineNames[i]));
			}
			else {
				//point to the station we already have of the same name
				tempLineStations.push_back(allLineStations.at(AScoords.at(0)).at(AScoords.at(1)));
			}


			tempLineStations.at(tempLineStations.size() - 1)->lines.push_back(lineNum);
			tempLineStations.at(tempLineStations.size() - 1)->linePos.push_back(i);
			tempLineVertices[i].color = sf::Color::Transparent;

			//save the successful station coordinates
			cxi = coords.x;
			cyi = coords.y;
		}
	}

	//do this once again at the end 
	tempLineVertices[i].position = sf::Vector2f((float)cxi, (float)cyi);
	tempLineNames.at(i) = std::to_string(cxi) + std::to_string(cyi);


	AScoords = alreadyStation(tempLineNames.at(i), allLineStations);
	if (AScoords == blank) {
		//make a new station
		tempLineStations.push_back(new Station(tempLineVertices[i], tempLineNames[i]));
		stationTotal++;
	}
	else {
		//point to the station we already have of the same name
		tempLineStations.push_back(allLineStations.at(AScoords.at(0)).at(AScoords.at(1)));
	}

	tempLineStations.at(tempLineStations.size() - 1)->lines.push_back(lineNum);
	tempLineStations.at(tempLineStations.size() - 1)->linePos.push_back(i);
	tempLineVertices[i].color = sf::Color::Transparent;



	mapImage.copy(statImage, coords.x - 5, coords.y - 5);

	allLineVertices.push_back(tempLineVertices);
	allLineNames.push_back(tempLineNames);
	allLineStations.push_back(tempLineStations);
	allLineTrains.push_back(tempLineTrains);


	for (auto it = tempLineStations.begin(); it != tempLineStations.end();) {
		it = tempLineStations.erase(it);
	}
	tempLineStations.clear();
}


//draws transfer stations where line1 and line2 intersect and adds them to the station, vertex, stationname vectors 
void City::findConnections(int line1, int line2) {
	static sf::Image statImage, whiteSquare; //I should probably do this somewhere else
	statImage.create(LINE_THICKNESS * 3, LINE_THICKNESS * 3, TRANSFER_COLOUR);
	whiteSquare.create(LINE_THICKNESS, LINE_THICKNESS, sf::Color::White);
	statImage.copy(whiteSquare, LINE_THICKNESS, LINE_THICKNESS);

	static sf::Vector2i blank = { -1, -1 };


	int i, j, k, lmin, ind;
	sf::Vector2i coords;
	Station *nStat, *temp;
	std::vector <Station*> statsToAdd, line1sorted, line2sorted;

	for (i = allLineStations.at(line1).size() - 2; i >= 0; i--) {//one before the end too
		for (j = allLineStations.at(line2).size() - 2; j >= 0; j--) {

			coords = findIntersection(allLineStations.at(line1).at(i)->position, allLineStations.at(line1).at(i + 1)->position, allLineStations.at(line2).at(j)->position, allLineStations.at(line2).at(j + 1)->position);
			if (coords != blank) {
				mapImage.copy(statImage, coords.x - 5, coords.y - 5);

				nStat = new Station(sf::Vector2f(coords), std::to_string(coords.x) + std::to_string(coords.y));
				nStat->lines = { line1, line2 };
				nStat->linePos = { i, j };
				
				//add the new transfer station to a vector to add them all later because otherwise it was reading the new transfer as a station in the findIntersection bit
				statsToAdd.push_back(nStat);
				stationTotal++;
			}
		}

	}


	line1sorted = statsToAdd;
	line2sorted = statsToAdd;

	//selection sort line1sorted in order of increasing position along line1
	for (i = 0; i < (int)line1sorted.size()-1; i++) {
		lmin = line1sorted.at(i)->linePos.at(line1);
		ind = i;
		for (j = i + 1; j < (int)line1sorted.size(); j++) {
			if (line1sorted.at(j)->linePos.at(line1) < lmin) {
				lmin = line1sorted.at(j)->linePos.at(line1);
				ind = j;
			}
		}
		if (lmin != line1sorted.at(i)->linePos.at(line1)) {
			temp = line1sorted.at(i);
			line1sorted.at(i) = line1sorted.at(ind);
			line1sorted.at(ind) = temp;
		}
	}

	//selection sort line2sorted in order of increasing position along line2
	for (i = 0; i < (int)line2sorted.size() - 1; i++) {
		lmin = line2sorted.at(i)->linePos.at(line2);
		ind = i;
		for (j = i + 1; j < (int)line2sorted.size(); j++) {
			if (line2sorted.at(j)->linePos.at(line2) < lmin) {
				lmin = line2sorted.at(j)->linePos.at(line2);
				ind = j;
			}
		}
		if (lmin != line2sorted.at(i)->linePos.at(line2)) {
			temp = line2sorted.at(i);
			line2sorted.at(i) = line2sorted.at(ind);
			line2sorted.at(ind) = temp;
		}
	}


	//iterate through stations for each line in order of decreasing position along that line
	for (ind = line1sorted.size()-1; ind >= 0; ind--){
		nStat = line1sorted.at(ind);
		i = nStat->linePos.at(line1)+1;

		insertVertex(allLineVertices.at(line1), nStat->position, i);
		allLineNames.at(line1).insert(allLineNames.at(line1).begin() + i, nStat->name);
		allLineStations.at(line1).insert(allLineStations.at(line1).begin() + i, nStat);

	}

	for (ind = line2sorted.size() - 1; ind >= 0; ind--) {
		nStat = line1sorted.at(ind);

		j = nStat->linePos.at(line2)+1;
		insertVertex(allLineVertices.at(line2), nStat->position, j);
		allLineNames.at(line2).insert(allLineNames.at(line2).begin() + j, nStat->name);
		allLineStations.at(line2).insert(allLineStations.at(line2).begin() + j, nStat);
	}

	//update the linePos for the other stations on each line
	for (j = 0; j < (int)allLineStations.at(line1).size(); j++) {
		for (k = 0; k < (int)allLineStations.at(line1).at(j)->lines.size(); k++) {
			if (allLineStations.at(line1).at(j)->lines.at(k) == line1) {
				allLineStations.at(line1).at(j)->linePos.at(k) = j;
			}
		}
	}
	for (j = 0; j < (int)allLineStations.at(line2).size(); j++) {
		for (k = 0; k < (int)allLineStations.at(line2).at(j)->lines.size(); k++) {
			if (allLineStations.at(line2).at(j)->lines.at(k) == line2) {
				allLineStations.at(line2).at(j)->linePos.at(k) = j;
			}
		}
	}
}


//runs setup on all trains in the city
void City::setUpTrains(void) {
	unsigned int i, j;
	for (i = 0; i < allLineTrains.size(); i++) {
		for (j = 0; j < allLineTrains.at(i).size(); j++) {
			allLineTrains.at(i).at(j).setup(allLineVertices.at(i));
		}
	}
}


/*
 *Generates a map based on perlin noise generation, then adds a station line and generates an image, texture, and sprite of the map as well as saving it to a file
 */
void City::generateMap(unsigned int seed) {
	srand(seed);
	randomGradVecs(Gradient); //make new random numbers in Gradient
	//this should probably be done with a perlin object or something

	int x, y;
	double p;
	sf::Texture texture;

	mapImage = generateImage(WINDOW_WIDTH * 2 / 3, WINDOW_HEIGHT, BOX_SIZE);

	for (x = 0; x < WINDOW_WIDTH * 2 /3 / BOX_SIZE; x++) {
		for (y = 0; y < WINDOW_HEIGHT / BOX_SIZE; y++) {
			p = perlin((double)x / SIMPLICITY / BOX_SIZE, (double)y / SIMPLICITY / BOX_SIZE);
			landType[x][y][0] = 0;
			landType[x][y][1] = (sf::Uint8)(255 - WATERLEVEL * p);
			landType[x][y][2] = (sf::Uint8)(p * WATERLEVEL);

			isWater[x][y] = landType[x][y][2] > landType[x][y][1];
			//sf::Color color(0, (sf::Uint8)(255 - WATERLEVEL * p), (sf::Uint8)(p*WATERLEVEL));
		}
	}

	
	generateStationLine(100, 50, 20, -20, 10, sf::Color::Black, "black", 5, 0);
	generateStationLine(20, -20, 100, 50, 20, sf::Color::Red, "red", 1, 1);
	generateStationLine(100, 50, 100, 50, 10, sf::Color::Blue, "blue", 1, 2);
	findConnections(0, 1);
	setUpTrains();

	/*
	generateStationLine(200, 50, 25, -25, sf::Color::Black, "black", 5, 0);
	generateStationLine(25, -25, 200, 50, sf::Color::Red, "red", 1, 1);
	generateStationLine(25, -25, 200, 50, sf::Color::Blue, "blue", 1, 2);
	*/colors = { "black","red", "blue"};
	
	std::ofstream outfile; 
	unsigned int i, j;

	mapName = "maps\\map" + std::to_string(seed);

	mapImage.saveToFile(mapName + ".png");
	outfile.open(mapName + ".txt");

	outfile << mapName + ".png" << std::endl;

	outfile << allLineTrains.size();
	for (i = 0; i < allLineTrains.size(); i++) {
		outfile << " " << colors.at(i) << " " << allLineTrains.at(i).size();
	}
	outfile << std::endl;

	for (i = 0; i < allLineStations.size(); i++) {
		outfile << allLineStations.at(i).size() << std::endl;

		for (j = 0; j < allLineStations.at(i).size(); j++) {
			outfile << allLineStations.at(i).at(j)->x << " " << allLineStations.at(i).at(j)->y << " \"" << allLineStations.at(i).at(j)->name << "\"" << std::endl;
		}
	}

	outfile.close();
	
	mapText.loadFromImage(mapImage);
	mapSprite.setTexture(mapText);
}


//Displays a window with the given sprite
void City::showSprite(const sf::Sprite &sprite) {
	sf::RenderWindow window(sf::VideoMode(WINDOW_WIDTH, WINDOW_HEIGHT), WINDOW_NAME);
	window.setFramerateLimit(FPS_MAX);
	sf::Event event;

	while (window.isOpen()) {// run the program as long as the window is open
		while (window.pollEvent(event)) {
			switch (event.type) {
				case sf::Event::Closed:
					window.close();
					break;
				default: {
					break;
				}
			}
		}


		window.clear(sf::Color::White);
		window.draw(sprite);
		window.display();
	}
}


//Passenger mechanics on where to go and drawing of passenger info text
void City::doPassengerMove(sf::RenderWindow &window) {
	for (unsigned int i = 0; i < passengers.size(); i++) {
		passengers.at(i).move(passengerTotal, allLineTrains, allLineStations);

		passengers.at(i).text.setString(passengers.at(i).genStatusText(colors, allLineTrains));
		passengers.at(i).text.setPosition((float)mapText.getSize().x, (float)((i + 1) * TEXT_SIZE_PASSENGER_INFO * 2));

		window.draw(passengers.at(i).text);
	}


}


//Moves all the trains in a city along their paths. This function is called once each iteration of the event loop
void City::doTrainMove(sf::RenderWindow &window) {
	unsigned int i, j;
	std::vector <Train> trains;

	for (j = 0; j < allLineTrains.size(); j++) {
		trains = allLineTrains.at(j); //for some reason it doesnt work unless it's done this way

		for (i = 0; i < trains.size(); i++) {
			trains.at(i).move(allLineVertices.at(j), allLineStations.at(j));
			trains.at(i).draw(window);

		}
		allLineTrains.at(j) = trains;

	}
}


//Draws all passengers in a city
void City::drawPassengers(sf::RenderWindow &window) {
	for (int i = passengers.size() - 1; i >= 0; i--) {

		if (passengers.at(i).status == PASSENGER_ARRIVED) {
			passengers.erase(passengers.begin() + i);
			continue;
		}
		else if (passengers.at(i).status == PASSENGER_RIDING || passengers.at(i).status == PASSENGER_NEARLY_DEST || passengers.at(i).status == PASSENGER_NEARLY_TRANSFER) {
			allLineTrains.at(passengers.at(i).currentTrain.at(0)).at(passengers.at(i).currentTrain.at(1)).passengerNumber++;
			passengers.at(i).shape.setPosition(allLineTrains.at(passengers.at(i).currentTrain.at(0)).at(passengers.at(i).currentTrain.at(1)).shape.getPosition());
		}
		window.draw(passengers.at(i).shape);
	}

}


//Draws the number of passengers on each train
void City::drawTrainPassengerNum(sf::RenderWindow &window) {
	std::vector<Train> trains;
	sf::FloatRect rect;
	unsigned int i, j;

	for (j = 0; j < allLineTrains.size(); j++) {

		trains = allLineTrains.at(j);

		for (i = 0; i<trains.size(); i++) {
			text.trainText.setString(std::to_string(trains.at(i).passengerNumber).c_str());
			rect = text.trainText.getLocalBounds();

			text.trainText.setOrigin(float(rect.width / 2.0), float(rect.height / 2.0));
			text.trainText.setPosition(trains.at(i).shape.getPosition().x, trains.at(i).shape.getPosition().y);

			window.draw(text.trainText);
		}
	}

}


/*
*takes a file, rectangle (for the train shape) and initializes the main components (five vectors) for a city and makes the sprite
*returns true or false based on success
*/
bool City::initializeFromFile(std::string cityFile) {
	int trainNum, segLen, xPos, yPos, trainsTotal, trainsPerLine;
	unsigned int i, j;

	std::vector <int> coords;
	std::vector <int> blank(2, -1);
	
	std::string color, stationWord;


	std::ifstream file(cityFile, std::ios::in);

	if (!file.is_open()) {
		return EXIT_FAILURE;
	}

	file >> mapName;
	file >> trainsTotal;

	for (trainNum = 0; trainNum < trainsTotal; trainNum++) {
		file >> color;
		file >> trainsPerLine;

		colors.push_back(color);
		allLineTrains.push_back(genLineTrains(trainsPerLine, trainShape, color));
	}

	for (j = 0; j < allLineTrains.size(); j++) {
		file >> segLen;

		sf::VertexArray tempLineVertices(sf::LinesStrip, segLen);
		std::vector<std::string> tempLineNames(tempLineVertices.getVertexCount());
		std::vector<Station*> tempLineStations;

		for (int i = 0; i < segLen; i++) {
			file >> xPos >> yPos >> stationWord;

			tempLineVertices[i].position = sf::Vector2f((float)xPos, (float)yPos);
			tempLineNames.at(i) = stationWord;

			coords = alreadyStation(stationWord, allLineStations);
			if (coords == blank) {
				//make a new station
				tempLineStations.push_back(new Station(tempLineVertices[i], tempLineNames[i]));
			}
			else {
				//point to the station we already have of the same name
				tempLineStations.push_back(allLineStations.at(coords.at(0)).at(coords.at(1)));
			}

			tempLineStations.at(tempLineStations.size() - 1)->lines.push_back(j);
			tempLineStations.at(tempLineStations.size() - 1)->linePos.push_back(i);
			tempLineVertices[i].color = sf::Color::Transparent;
		}

		for (i = 0; i < allLineTrains.at(j).size(); i++) {
			allLineTrains.at(j).at(i).setup(tempLineVertices);
		}

		allLineVertices.push_back(tempLineVertices);
		allLineNames.push_back(tempLineNames);
		allLineStations.push_back(tempLineStations);

		for (auto it = tempLineStations.begin(); it != tempLineStations.end();) {
			it = tempLineStations.erase(it);
		}
		tempLineStations.clear();
	}

	file.close();


	if (!mapText.loadFromFile(mapName)) {
		return EXIT_FAILURE;
	}
	mapSprite.setTexture(mapText);

	return EXIT_SUCCESS;
}


//takes the coordinates of two (consecutive) stations from two different lines and returns the location at which they cross ({-1,-1} if they don't cross)
//note: it takes vector2f but returns vector2i
sf::Vector2i findIntersection(sf::Vector2f line1_1, sf::Vector2f line1_2, sf::Vector2f line2_1, sf::Vector2f line2_2) {
	double A1, A2, B1, B2, C1, C2, det, xf, yf;
	static sf::Vector2i blank = { -1,-1 };

	//convert two lines from (x1,y1), (x2,y2) form to Ax+By=C forms

	A1 = line1_2.y - line1_1.y;
	B1 = line1_1.x - line1_2.x;
	C1 = A1 * line1_1.x + B1 * line1_1.y;

	A2 = line2_2.y - line2_1.y;
	B2 = line2_1.x - line2_2.x;
	C2 = A2 * line2_1.x + B2 * line2_1.y;

	det = A1 * B2 - A2 * B1;

	if (fabs(det) <= 0.001) { //threshold to allow for double imprecisions
		return blank;
	}
	else {
		xf = (B2*C1 - B1 * C2) / det;
		yf = (A1*C2 - A2 * C1) / det;

		//if the intersection is out of range
		//I should probably look at redoing this float comparison thing someday
		if (xf < fmin(line1_1.x, line1_2.x) || xf > fmax(line1_1.x, line1_2.x) || yf < fmin(line1_1.y, line1_2.y) || yf > fmax(line1_1.y, line1_2.y) || xf < fmin(line2_1.x, line2_2.x) || xf > fmax(line2_1.x, line2_2.x) || yf < fmin(line2_1.y, line2_2.y) || yf > fmax(line2_1.y, line2_2.y)) {
			return blank;
		}
		else {
			return sf::Vector2i((int)xf, (int)yf);
		}
	}
}


void lineBetween(double x1, double y1, double x2, double y2, sf::Image &background, sf::Color colour) {
	
	sf::Image blackSquare, whiteSquare;
	blackSquare.create(LINE_THICKNESS, LINE_THICKNESS, colour);
	whiteSquare.create(LINE_THICKNESS, LINE_THICKNESS, sf::Color::White);

	double slope = (y2 - y1) / (x2 - x1);

	if (fabs(slope) > 1) {
		slope = 1.0 / slope;

		if (y1 < y2)
			for (int y = (int)y1; y < (int)y2; y++)
				background.copy(blackSquare,  (unsigned int)(slope*(y - y1) + x1), (unsigned int)y);

		else if (y1 > y2)
			for (int y = (int)y2; y < (int)y1; y++)
				background.copy(blackSquare, (unsigned int)(slope*(y - y1) + x1), (unsigned int)y);
	}
	else {
		if (x1 < x2)
			for (int x = (int)x1; x < (int)x2; x++)
				background.copy(blackSquare, (unsigned int)x, (unsigned int)(slope*(x - x1) + y1));

		else if (x1 > x2)
			for (int x = (int)x2; x < (int)x1; x++)
				background.copy(blackSquare, (unsigned int)x, (unsigned int)(slope*(x - x1) + y1));
	}

	background.copy(whiteSquare, (unsigned int)x1, (unsigned int)y1);
	background.copy(whiteSquare, (unsigned int)x2, (unsigned int)y2);

}


//inserts a new vertex with position newCoords into VertexArray va at index pos
void insertVertex(sf::VertexArray &va, sf::Vector2f newCoords, int pos) {
	sf::VertexArray vf(sf::LinesStrip);
	int v;
	for (v = 0; v < (int)va.getVertexCount(); v++) {
		if (v == pos)
			vf.append(sf::Vertex(newCoords, sf::Color::Transparent));
		vf.append(va[v]);
	}
	va = vf;
}


void randomGradVecs(double Gradient[WINDOW_WIDTH / BOX_SIZE][WINDOW_HEIGHT / BOX_SIZE][2] ) {
	int x, y;
	for (x = 0; x < WINDOW_WIDTH / BOX_SIZE; x++) {
		for (y = 0; y < WINDOW_HEIGHT / BOX_SIZE; y++) {
			Gradient[x][y][0] = rand() % 1000 / 1000.0;
			Gradient[x][y][1] = rand() % 1000 / 1000.0;
		}
	}

}


/*
 *linearly interpolate between a0 and a1
 *weight w should be between 0 and 1
 */
double lerp(double a0, double a1, double w) {
	return (1.0 - w)*a0 + w*a1;
}


// Computes the dot product of the distance and gradient vectors.
double dotGridGradient(int ix, int iy, double x, double y) {
	   
	// Precomputed (or otherwise) gradient vectors at each grid node
	
	// Compute the distance vector
	double dx = x - (double)ix;
	double dy = y - (double)iy;

	// Compute the dot-product
	return (dx*Gradient[iy][ix][0] + dy*Gradient[iy][ix][1]);
}


// Compute Perlin noise at coordinates x, y
double perlin(double x, double y) {

	// Determine grid cell coordinates
	int x0 = (int)floor(x);
	int x1 = x0 + 1;
	int y0 = (int)floor(y);
	int y1 = y0 + 1;

	// Determine interpolation weights
	// Could also use higher order polynomial/s-curve here
	double sx = x - (double)x0;
	double sy = y - (double)y0;

	// Interpolate between grid point gradients
	double n0, n1, ix0, ix1, value;
	n0 = dotGridGradient(x0, y0, x, y);
	n1 = dotGridGradient(x1, y0, x, y);
	ix0 = lerp(n0, n1, sx);
	n0 = dotGridGradient(x0, y1, x, y);
	n1 = dotGridGradient(x1, y1, x, y);
	ix1 = lerp(n0, n1, sx);
	value = lerp(ix0, ix1, sy);

	return value;
}


//returns a random integer, an multiple of step above min, between min and max inclusive
int randInt(int min, int max, int step) {
	return (rand() % (max - min + 1) + min) / step * step;
}


void swap(int &a, int &b) {
	int temp = b;
	b = a;
	a = temp;
}
