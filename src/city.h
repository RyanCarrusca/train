#pragma once

#include "train.h"
#include "station.h"
#include "passenger.h"
#include "text.h"

#ifndef CITY_H
#define CITY_H

#define WINDOW_WIDTH 1500
#define WINDOW_HEIGHT 1000

#define FPS_MAX 60

#define PASSENGER_FREQUENCY 120
#define TRAIN_IMAGE "assets/train.png"
#define FONT_FILE "assets/Raleway-Regular.ttf"

#define WINDOW_NAME "train"


#define BOX_SIZE 25
#define SIMPLICITY 1
#define WATERLEVEL 12

#define TRANSFER_COLOUR sf::Color::Yellow


class City {
public:
	sf::RenderWindow window;

	sf::Image mapImage;
	sf::Texture mapText;
	sf::Sprite mapSprite;
	std::string mapName;

	Text text;

	sf::Texture trainTexture;
	sf::RectangleShape trainShape;

	/* RGB
	*if 3>2, it's water
	*if 2>3, it's land
	*/
	sf::Uint8 landType[WINDOW_WIDTH * 2 / 3 / BOX_SIZE][WINDOW_HEIGHT / BOX_SIZE][3];

	/*checks if water is at  x, y */
	bool isWater[WINDOW_WIDTH * 2 / 3 / BOX_SIZE][WINDOW_HEIGHT / BOX_SIZE];

	std::vector <std::vector <Train> > allLineTrains;
	std::vector <sf::VertexArray> allLineVertices;
	std::vector <std::vector<std::string> > allLineNames;
	std::vector <std::vector <Station*> > allLineStations;
	std::vector <std::string> colors;

	std::vector <Passenger> passengers;

	int passengerTotal;
	int stationTotal;

	City();
	~City();

	void run(void);

	void generateStationLine(int max_dist_x, int min_dist_x, int max_dist_y, int min_dist_y, int numStations, sf::Color colour, std::string color, int trainsPerLine, int lineNum);
	void findConnections(int line1, int line2);
	void setUpTrains(void);
	void generateMap(unsigned int seed);
	bool initializeFromFile(std::string cityFile);


	void showSprite(const sf::Sprite &sprite);

	void doPassengerMove(sf::RenderWindow &window);
	void doTrainMove(sf::RenderWindow &window);
	void drawPassengers(sf::RenderWindow &window);
	void drawTrainPassengerNum(sf::RenderWindow &window);



};


sf::Image generateImageBox(double x, double y);
sf::Image generateImage(int width, int height, int boxSize);

sf::Vector2i findIntersection(sf::Vector2f line1_1, sf::Vector2f line1_2, sf::Vector2f line2_1, sf::Vector2f line2_2);
void lineBetween(double x1, double y1, double x2, double y2, sf::Image &background, sf::Color colour);
void insertVertex(sf::VertexArray &va, sf::Vector2f newCoords, int pos);
void randomGradVecs(double Gradient[WINDOW_WIDTH / BOX_SIZE][WINDOW_HEIGHT / BOX_SIZE][2]);

double lerp(double a0, double a1, double w);
double dotGridGradient(int ix, int iy, double x, double y);
double perlin(double x, double y);
int randInt(int min, int max, int step=1);
void swap(int &a, int &b);

#endif
