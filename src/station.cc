#include "station.h"


Station::Station() {};


Station::Station(sf::Vertex vertx, std::string nam) {
	vert = vertx;
	position = vertx.position;
	name = nam;
	y = (int)vertx.position.y;
	x = (int)vertx.position.x;
}


/*
*Returns a random Station.
*Parameter: 2D Station* vector
*/
Station getRandStation(const std::vector <std::vector <Station*> > &allLineStations) {
	int randLine = rand() % allLineStations.size();
	int randStat = rand() % allLineStations.at(randLine).size();

	return *allLineStations.at(randLine).at(randStat);
}

/*
*Determines if the given station name already exists
*Parameter: stationWord - string,
*Parameter: 2D vector of Station* - all stations
*/
std::vector<int> alreadyStation(std::string stationWord, std::vector <std::vector <Station*> > allLineStations) {
	std::vector <int> coords(2, -1);
	for (unsigned x = 0; x<allLineStations.size(); x++) {
		for (unsigned y = 0; y<allLineStations.at(x).size(); y++) {
			if (stationWord == allLineStations.at(x).at(y)->name) {
				coords.at(0) = x;
				coords.at(1) = y;
				return coords;
			}
		}
	}
	return coords;
}


/*
*Finds the best path from Station start to Station end using Dijkstra's algorithm
*/
void findTransfer(Station start, Station end, Station &transfer, int &next, std::vector<std::vector<Station*> > allLineStations) {
	std::vector<Station*> q;
	q.push_back(&start);
	for (unsigned i = 0; i<allLineStations.size(); i++) {
		for (unsigned j = 0; j<allLineStations.at(i).size(); j++) {
			allLineStations.at(i).at(j)->dist = 100000;
			allLineStations.at(i).at(j)->prev = 0;
			if (allLineStations.at(i).at(j)->name == start.name) {
				q.at(0) = allLineStations.at(i).at(j);
			}
			else {
				q.push_back(allLineStations.at(i).at(j));
			}
		}
	}
	q.at(0)->dist = 0;

	std::vector <Station> queue;
	double ldist, alt;
	int ind, linemax;
	while (q.size()) {
		ldist = q.at(0)->dist;
		ind = 0;
		for (unsigned i = 1; i<q.size(); i++) {
			if (q.at(i)->dist < ldist) {
				ldist = q.at(i)->dist;
				ind = i;
			}
		}
		Station* u = q.at(ind);
		q.erase(q.begin() + ind);

		std::vector<Station*> nearby(0);
		for (unsigned i = 0; i<u->lines.size(); i++) { //finds all neighbours of station u
			Station *statL, *statR;
			bool inQL = false, inQR = false, unQL = false, unQR = false;
			linemax = allLineStations.at(u->lines.at(i)).size() - 1;
			if (u->linePos.at(i) > 0) {
				statL = allLineStations.at(u->lines.at(i)).at(u->linePos.at(i) - 1);
				unQL = true;
			}
			if (u->linePos.at(i) < linemax) {
				statR = allLineStations.at(u->lines.at(i)).at(u->linePos.at(i) + 1);
				unQR = true;
			}
			for (unsigned j = 0; j<q.size(); j++) {
				if (unQL && (q.at(j)->name == statL->name)) {
					inQL = true;
				}
				else if (unQR && (q.at(j)->name == statR->name)) {
					inQR = true;
				}
			}
			if (inQL) {
				nearby.push_back(statL);
			}if (inQR) {
				nearby.push_back(statR);
			}
		}

		for (unsigned i = 0; i<nearby.size(); i++) {
			alt = u->dist + hypot(u->x - nearby.at(i)->x, u->y - nearby.at(i)->y);
			if (alt < nearby.at(i)->dist) {
				nearby.at(i)->dist = alt;
				nearby.at(i)->prev = u;
			}
		}

		if (u->name == end.name) {
			while (u->prev) {
				queue.push_back(*u);
				u = u->prev;
			}
			queue.push_back(*u);
			break;
		}
	}

	if (queue.size() == 2) {
		std::vector<int>::iterator it;
		std::vector<int> intersect(queue.at(0).lines.size());
		std::vector <int> q0 = queue.at(0).lines;
		std::vector<int> q1 = queue.at(1).lines;
		it = std::set_intersection(q0.begin(), q0.begin() + q0.size(), q1.begin(), q1.begin() + q1.size(), intersect.begin());
		intersect.resize(it - intersect.begin());
		next = intersect.at(0);
		transfer = queue.at(1);
		return;
	}
	std::vector<int> linesRemaining = queue.at(queue.size() - 1).lines;


	for (int i = queue.size() - 2; i >= 0; i--) {
		std::vector<bool> toKeep(linesRemaining.size(), false);
		for (unsigned j = 0; j<queue.at(i).lines.size(); j++) {
			for (unsigned k = 0; k<linesRemaining.size(); k++) {
				if (queue.at(i).lines.at(j) == linesRemaining.at(k)) {
					toKeep.at(k) = true;
					break;
				}
			}
		}
		std::vector <int> temp;
		for (unsigned i = 0; i<linesRemaining.size(); i++) {
			if (toKeep.at(i)) {
				temp.push_back(linesRemaining.at(i));
			}
		}

		if (temp.size() == 0) {
			next = linesRemaining.at(0);
			transfer = queue.at(i + 1);
			return;
		}
		linesRemaining = temp;
	}
	next = linesRemaining.at(0);
	transfer = queue.at(0);
	return;
}
