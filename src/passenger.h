#pragma once

#include "train.h"


#define PASSENGER_WAITING 0
#define PASSENGER_RIDING 1
#define PASSENGER_ARRIVED 2
#define PASSENGER_NEARLY_DEST 3
#define PASSENGER_NEARLY_TRANSFER 4

#define PASSENGER_COLOUR sf::Color::Cyan

#ifndef PASSENGER_H
#define PASSENGER_H


class Passenger {
public:
	sf::Vector2f position;
	Station destination;
	Station start;
	Station transfer;
	Station currentStation;
	std::vector<int> currentTrain;

	int next;
	int status;
	int ssn;

	sf::CircleShape shape;
	sf::Text text;

	Passenger();
	Passenger(sf::Vector2f pos, Station st, Station end);
	Passenger(std::vector <std::vector <Station*> > &allLineStations, int sn);

	void move(int &passengerTotal, std::vector <std::vector <Train> > &allLineTrains, std::vector<std::vector<Station*> > allLineStations);
	std::string genStatusText(const std::vector<std::string> &colors, const std::vector <std::vector <Train> > &allLineTrains);
};


#endif
